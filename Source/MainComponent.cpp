/*
 ==============================================================================
 
 This file was auto-generated!
 
 ==============================================================================
 */

#ifndef MAINCOMPONENT_H_INCLUDED
#define MAINCOMPONENT_H_INCLUDED

#include "../JuceLibraryCode/JuceHeader.h"

//==============================================================================
/*
 This component lives inside our window, and this is where you should put all
 your controls and content.
 */
class MainContentComponent   : public AudioAppComponent
{
public:
    //==============================================================================
    MainContentComponent()
    {
        
        levelSlider.setRange (0.0, 1.0);
        levelSlider.setTextBoxStyle (Slider::TextBoxRight, false, 100, 20);
        levelLabel.setText ("Volume Level", dontSendNotification);
        
        panningSlider.setRange(-1.0, 1.0);
        panningSlider.setTextBoxStyle(Slider::TextBoxRight, false, 100, 20);
        panningLabel.setText ("Pan L-R", dontSendNotification);
        
        distortionSlider.setRange (0.01, 1);
        distortionSlider.setTextBoxStyle (Slider::TextBoxRight, false, 100, 20);
        distortionLabel.setText ("Overdrive min-max", dontSendNotification);
        
        addAndMakeVisible (levelSlider);
        addAndMakeVisible (levelLabel);
        addAndMakeVisible (panningSlider);
        addAndMakeVisible (panningLabel);
        addAndMakeVisible(distortionLabel);
        addAndMakeVisible (distortionSlider);
        //window size
        setSize (800, 600);
        
        // specify the number of input and output channels that we want to open
        setAudioChannels (2, 2);
    }
    
    ~MainContentComponent()
    {
        shutdownAudio();
    }
    
    //==============================================================================
    void prepareToPlay (int samplesPerBlockExpected, double sampleRate) override
    {
        // This function will be called when the audio device is started, or when
        // its settings (i.e. sample rate, block size, etc) are changed.
        
        // You can use this function to initialise any resources you might need,
        // but be careful - it will be called on the audio thread, not the GUI thread.
        
        // For more details, see the help for AudioProcessor::prepareToPlay()
    }
    
    void getNextAudioBlock (const AudioSourceChannelInfo& bufferToFill) override
    {
        // Your audio-processing code goes here!
        AudioIODevice* device = deviceManager.getCurrentAudioDevice();
        const BigInteger activeInputChannels = device->getActiveInputChannels();
        const BigInteger activeOutputChannels = device->getActiveOutputChannels();
        const int maxInputChannels = activeInputChannels.getHighestBit() + 1;
        const int maxOutputChannels = activeOutputChannels.getHighestBit() + 1;
        
        const float level = (float) levelSlider.getValue();
        const float pan = (float) panningSlider.getValue();
        const float distor = (float) distortionSlider.getValue();
        
        for (int channel = maxOutputChannels-1; channel >= 0; --channel){
            if ((! activeOutputChannels[channel]) || maxInputChannels == 0)
            {
                bufferToFill.buffer->clear (channel, bufferToFill.startSample, bufferToFill.numSamples);
            }
            else
            {
                const int actualInputChannel = 0;//for the first input source
                
                if (! activeInputChannels[channel]) // output silence if input channel is set to inactive
                {
                    bufferToFill.buffer->clear (channel, bufferToFill.startSample, bufferToFill.numSamples);
                }
                else // applies the processing
                {
                    const float* inBuffer = bufferToFill.buffer->getReadPointer (actualInputChannel,
                                                                                 bufferToFill.startSample); //pointer to input samples
                    float* outBuffer = bufferToFill.buffer->getWritePointer (channel, bufferToFill.startSample); //pointer to outbuffer, for channel 0 the memory direction is the same as inBuffer, so we apply the processing to channel 1 (left) first
                    
                  
                    
                    
                    if (channel==0){ //left output
                        
                        double highvalue=pow((1-distor),2); //pow 2 so as to achieve a log increase
                        double lowvalue=(-1)*pow((1-distor),2);
                        
                        for (int sample = 0; sample < bufferToFill.numSamples; ++sample){
                             //apply the distortion by clipping the signal
                            
                                if(inBuffer[sample]>highvalue){ //clipping between 0 and 1
                                    
                                    outBuffer[sample]=1-distor;
                                }
                                
                                if(inBuffer[sample]<lowvalue){ //clipping between 0 and -1
                                    
                                    outBuffer[sample]=-1*distor;
                                }
                                else outBuffer[sample]=inBuffer[sample];
                                
                            
                        //apply level
                            outBuffer[sample] = outBuffer[sample] * 2.0 *level * (fabs(pan - 1.0));//pan
                            
                        }
                    }
                    
                    else if (channel==1){ //right output
                        double highvalue=pow((1-distor),2); //pow 2 so as to achieve a log increase
                        double lowvalue=(-1)*pow((1-distor),2);

                        for (int sample = 0; sample < bufferToFill.numSamples; ++sample){
                            
                            //apply the distortion by clipping the signal
                            
                            if(inBuffer[sample]>highvalue){ //clipping between 0 and 1
                                
                                outBuffer[sample]=1-distor;
                            }
                            
                            if(inBuffer[sample]<lowvalue){ //clipping between 0 and -1
                                
                                outBuffer[sample]=-1*distor;
                            }
                            
                            else outBuffer[sample]=inBuffer[sample];
                            
                            //level
                            outBuffer[sample] = outBuffer[sample] * 2.0 *level * (fabs(pan + 1.0));
                        }
                    }
                }
            }
        }
    }
    
    // For more details, see the help for AudioProcessor::getNextAudioBlock()
    
    
    
    void releaseResources() override
    {
        // This will be called when the audio device stops, or when it is being
        // restarted due to a setting change.
        
        // For more details, see the help for AudioProcessor::releaseResources()
    }
    
    //==============================================================================
    void paint (Graphics& g) override
    {
        // (Our component is opaque, so we must completely fill the background with a solid colour)
        g.fillAll (Colours::white);
        
        
        // You can add your drawing code here!
    }
    
    void resized() override
    {
        // This is called when the MainContentComponent is resized.
        // If you add any child components, this is where you should
        // update their positions.
        
        distortionLabel.setBounds (10, 250, 490, 80);
        distortionSlider.setBounds (140, 250, getWidth() - 150, 80);
        levelLabel.setBounds (10, 10, 90, 20);
        levelSlider.setBounds (100, 10, getWidth() - 110, 20);
        panningLabel.setBounds (30, 130, 290, 50);
        panningSlider.setBounds (100, 130, getWidth() - 110, 50);
        
    }
    
    
private:
    //==============================================================================
    
    // Your private member variables go here...
    
    Label levelLabel;
    Label panningLabel;
    Label distortionLabel;
    Slider levelSlider;
    Slider panningSlider;
    Slider distortionSlider;
    
    
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MainContentComponent)
};


// (This function is called by the app startup code to create our main component)
Component* createMainContentComponent()     { return new MainContentComponent(); }


#endif  // MAINCOMPONENT_H_INCLUDED
