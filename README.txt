This project uses the JUCE library and modules to manage the audio processing.

It consists of a panner, an overdrive effect and a volume.

It is aimed for live use, it takes the audio input and output from the computer preferences and treats it so that you can control the three parameters (pan, volume, overdrive).

It has been only tested on a MacBook Pro (late 2011) with macOS Sierra.

